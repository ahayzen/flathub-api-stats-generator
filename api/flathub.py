"""
Copyright (C) 2018, 2019
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import date
from os import path

from api import BaseAPI


class FlathubAPIv1(BaseAPI):
    BASE_URL = "https://flathub.org/api/v1/"
    BASE_FRONT_STATS_URL = "https://flathub.org/stats/"
    DATETIME_FORMAT = ["%Y-%m-%dT%H:%M:%SZ", "%Y-%m-%dT%H:%M:%S.%fZ"]
    FIELD_CREATION_DATE = "inStoreSinceDate"
    FIELD_ID = "flatpakAppId"
    NAME = "Flathub"

    @classmethod
    def get_apps(cls):
        return cls.get_json_data(cls.BASE_URL, "apps")

    @classmethod
    def get_app_by_id(cls, app_id):
        return cls.get_json_data(cls.BASE_URL, "apps", app_id)

    @classmethod
    def get_app_by_record(cls, record):
        # Flathub has enough info in the record from get_apps
        # so here we can just return the record back :-)
        return record

    @classmethod
    def get_downloads_for_date(cls, year=2018, month=4, day=29):
        return cls.get_json_data(
            cls.BASE_FRONT_STATS_URL,
            path.join(
                "%d" % year,
                ("%d" % month).rjust(2, "0"),
                "%s.json" % (("%d" % day).rjust(2, "0"))
            ),
            can_cache=True,
        )
