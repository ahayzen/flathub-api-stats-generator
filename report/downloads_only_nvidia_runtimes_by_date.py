"""
Copyright (C) 2018
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime

from . import BaseReport, IS_NVIDIA, IS_NVIDIA_32
from model import DownloadModel


class DownloadsOnlyNvidiaRuntimesByDateReport(BaseReport):
    HAS_DATA_STRING = True
    HAS_DATA_ZIPPED = True
    MODEL = DownloadModel

    def __init__(self, model, args=[]):
        BaseReport.__init__(self, model)

        # Pre read all the nvidia apps
        self.runtimes = []

        skip_32 = False
        skip_64 = False

        for arg in args:
            try:
                key, value = arg
            except ValueError:
                print("Expected exactly one equals, got these parts: %s" % arg)
                break

            if key == "skip-nvidia-32":
                skip_32 = True
            elif key == "skip-nvidia-64":
                skip_64 = True
            else:
                print("Unknown report-arg %s=%s" % (key, value))

        for _, data in self.model.downloads_by_date():
            for runtime in data["refs"]:
                if (runtime not in self.runtimes
                        and ((not skip_64 and self.is_nvidia_name(runtime))
                             or (not skip_32 and self.is_nvidia_32_name(runtime)))):
                    self.runtimes.append(runtime)

        self.runtimes.sort(reverse=True)

    @property
    def axis(self):
        return ["Time", "Downloads"]

    def data_string(self):
        return [
            "%s\t%i\t%i\t%i" % (
                runtime,
                self.downloads_in_slice(runtime, self.model.downloads_by_date()),
                self.downloads_in_slice(runtime, self.model.downloads_by_date()[-30:]),
                self.downloads_in_slice(runtime, self.model.downloads_by_date()[-7:]),
            ) for runtime in self.runtimes
        ]

    def data_zipped(self):
        return [
            zip(
                *(
                    [
                        date,
                        sum(
                            data["refs"][runtime][arch][0]
                            for arch in data["refs"].get(runtime, [])
                        )
                    ]
                    for date, data in self.model.downloads_by_date()
                )
            ) for runtime in self.runtimes
        ]

    def data_zipped_kwargs(self):
        return [
            {
                "label": runtime,
                "x_date": True
            } for runtime in self.runtimes
        ]

    @staticmethod
    def downloads_in_slice(runtime, s):
        return sum(
            sum(
                data["refs"][runtime][arch][0]
                for arch in data["refs"].get(runtime, [])
            )
            for date, data in s
        )

    @staticmethod
    def is_nvidia_name(name):
        return IS_NVIDIA.match(name)

    @staticmethod
    def is_nvidia_32_name(name):
        return IS_NVIDIA_32.match(name)

    @property
    def title(self):
        return (
            "Downloads by Nvidia runtime version in %s over time - %s" % (
                self.model.api.NAME,
                datetime.strftime(datetime.utcnow(), "%Y-%m-%d %H:%M:%S")
            )
        )
