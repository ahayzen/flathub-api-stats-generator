"""
Copyright (C) 2018
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from . import BaseReport
from model import AppModel


class AppsByCreationDateReport(BaseReport):
    HAS_DATA_STRING = True
    HAS_DATA_ZIPPED = False
    MODEL = AppModel

    def __init__(self, model, args=[]):
        BaseReport.__init__(self, model)

        for arg in args:
            try:
                key, value = arg
            except ValueError:
                print("Expected exactly one equals, got these parts: %s" % arg)
                break

            if key == "app-id":
                print("list_by_date does not support app-id filtering")
            else:
                print("Unknown report-arg %s=%s" % (key, value))

    def data_string(self):
        return (
            "%s\t%s" % (data["creationDate"].isoformat(timespec="minutes"),
                        data["id"])
            for data in self.model.apps_ordered_by("creationDate")
        )
